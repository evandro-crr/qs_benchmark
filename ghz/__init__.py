from graph import plot
from qsystem import QSystem
from qiskit import QuantumCircuit
from qiskit import BasicAer, execute
from pyquil.quil import Program
from pyquil.gates import H, CNOT, MEASURE
from pyquil.api import QVMConnection
from projectq import MainEngine, ops
import subprocess
import cirq

def qsys(size): # QSystem bitwise
    q = QSystem(size, representation='bitwise')
    q.evol('H', 0)
    for j in range(size-1):
        q.cnot(j+1, [0])
    q.measure_all()

def qkit(size): # Qiskit qasm_simulator
    qc = QuantumCircuit(size, size, name='ghz')
    qc.h(0)
    for i in range(size-1):
        qc.cx(0, i+1)

    for i in range(size):
        qc.measure(i, i)

    sim_backend = BasicAer.get_backend('qasm_simulator')
    job = execute(qc, sim_backend, shots=1)
    job.result()

def forest(size): # Forest QVM
    p = Program(H(0)) 
    ro = p.declare('ro', 'BIT', size)

    for j in range(size-1):
        p += CNOT(0, j+1)
        
    for j in range(size):
        p += MEASURE(j, ro[j])

    qvm = QVMConnection()
    qvm.wavefunction(p)

def crq(size): # Cirq
    q = [cirq.LineQubit(i) for i in range(size)]
    def cnots():
        for i in range(size-1):
            yield cirq.CNOT(q[0], q[i+1])

    circuit = cirq.Circuit()
    circuit.append(cirq.H(q[0]))
    circuit.append(cnots())
    
    circuit.append(cirq.measure(cirq.LineQubit(i)) for i in range(size))

    simulator = cirq.Simulator()
    simulator.run(circuit)

def projq(size): # ProjectQ
    eng = MainEngine()
    q = [eng.allocate_qubit() for _ in range(size)]
    ops.H | q[0]
    for i in q[1:]:
        ops.CNOT | (q[0], i)
    for i in q:
        ops.Measure | i
    eng.flush()

def qrack(size):
    subprocess.call(['qrack/build/examples/ghz', str(size)])
    
def quest(size):
    subprocess.call(['QuEST/ghz' , str(size)])

ghz = {'QSystem' : qsys,
       'Cirq' : crq,
       'Forest' : forest,
       'ProjectQ' : projq,
       'Qiskit' : qkit, 
       'Qrack' : qrack,
       'QuEST' : quest,
       } 

def call_plots():
    plot('ghz.pdf', ghz, list(range(2, 25)))
