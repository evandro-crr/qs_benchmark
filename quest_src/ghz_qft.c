#include "QuEST.h"
#include <stdlib.h>

int main(int argc, char* argv[]) {

    QuESTEnv env = createQuESTEnv();

    int size = atoi(argv[1]);

    Qureg q = createQureg(size, env);

    initZeroState(q);

    hadamard(q, 0);

    for (int i = 1; i < size; i++) 
        controlledNot(q, 0, i);
    
    for (int i = 0; i < size/2; i++) {
        hadamard(q, i);
        int m = 1;
        for (int j = i+1; j < size/2; j++) {
            double theta = M_PI/pow(2.0, m); 
            controlledPhaseShift(q, j, i, theta);
            m++;
        }
    }

    for (int i = 0; i < size; i++) 
        measure(q, i);

    destroyQuESTEnv(env);
}