#include "QuEST.h"
#include <stdlib.h>

int main(int argc, char* argv[]) {

    QuESTEnv env = createQuESTEnv();

    int size = atoi(argv[1]);

    Qureg q = createQureg(size, env);

    initZeroState(q);

    for (int i = 0; i < size; i++) 
        hadamard(q, i);
        
    for (int i = 0; i < size; i++) 
        measure(q, i);

    destroyQuESTEnv(env);
}