from graph import plot
from qsystem import QSystem
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister
from qiskit import BasicAer, execute
from qiskit.circuit.library import basis_change
from pyquil.quil import Program
from pyquil.gates import H, CPHASE, MEASURE
from pyquil.api import QVMConnection
from projectq import MainEngine, ops
import subprocess
import cirq
from math import pi

def qsys(size): # QSystem bitwise
    q = QSystem(size, representation='bitwise')
    q.qft(0, size)
    q.measure_all()

def qkit(size): # Qiskit qasm_simulator
    qr = QuantumRegister(size)
    cr = ClassicalRegister(size)
    qc = QuantumCircuit(qr, cr)

    ft = basis_change.QFT()
    ft.add_register(qr)

    qc.extend(ft)

    for i in range(size):
        qc.measure(i, i)

    sim_backend = BasicAer.get_backend('qasm_simulator')
    job = execute(qc, sim_backend, shots=1)
    job.result()


def forest(size): # Forest QVM
    p = Program() 
    ro = p.declare('ro', 'BIT', size)

    lambd = [pi/pow(2, m) for m in range(1, size)]
    for i in range(size):
        p += H(i)
        for j, l in zip(range(i+1, size), lambd):
            p += CPHASE(l, j, i)

    for j in range(size):
        p += MEASURE(j, ro[j])

    qvm = QVMConnection()
    qvm.wavefunction(p)

def crq(size): # Cirq
    q = [cirq.LineQubit(i) for i in range(size)]

    circuit = cirq.Circuit()

    circuit.append(cirq.ops.qft(*(q[i] for i in range(size))))
    
    circuit.append(cirq.measure(cirq.LineQubit(i)) for i in range(size))

    simulator = cirq.Simulator()
    result = simulator.run(circuit)
    result.measurements


def projq(size): # ProjectQ
    eng = MainEngine()
    q = [eng.allocate_qubit() for _ in range(size)]
    
    ops.QFT | (i for i in q)

    for i in q:
        ops.Measure | i
    eng.flush()


def qrack(size):
    subprocess.call(['qrack/build/examples/qft', str(size)])
    
def quest(size):
    subprocess.call(['QuEST/qft' , str(size)])

h = {'QSystem' : qsys,
     'Cirq' : crq,
     'Forest' : forest,
     'ProjectQ' : projq,
     'Qiskit' : qkit, 
     'Qrack' : qrack,
     'QuEST' : quest,
     } 

def call_plots():
    plot('qft.pdf', h, list(range(2, 25)))
