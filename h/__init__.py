from graph import plot
from qsystem import QSystem
from qiskit import QuantumCircuit
from qiskit import BasicAer, execute
from pyquil.quil import Program
from pyquil.gates import H, CNOT, MEASURE
from pyquil.api import QVMConnection
from projectq import MainEngine, ops
import subprocess
import cirq

def qsys(size): # QSystem bitwise
    q = QSystem(size, representation='bitwise')
    q.evol('H', 0, size)
    q.measure_all()

def qkit(size): # Qiskit qasm_simulator
    qc = QuantumCircuit(size, size, name='ghz')
    for i in range(size):
        qc.h(i)

    for i in range(size):
        qc.measure(i, i)

    sim_backend = BasicAer.get_backend('qasm_simulator')
    job = execute(qc, sim_backend, shots=1)
    job.result()

def forest(size): # Forest QVM
    p = Program() 
    ro = p.declare('ro', 'BIT', size)

    for j in range(size):
        p += H(j)
        
    for j in range(size):
        p += MEASURE(j, ro[j])

    qvm = QVMConnection()
    qvm.wavefunction(p)

def crq(size): # Cirq
    q = [cirq.LineQubit(i) for i in range(size)]

    circuit = cirq.Circuit()
    circuit.append(cirq.H(q[i]) for i in range(size))
    
    circuit.append(cirq.measure(cirq.LineQubit(i)) for i in range(size))

    simulator = cirq.Simulator()
    simulator.run(circuit)

def projq(size): # ProjectQ
    eng = MainEngine()
    q = [eng.allocate_qubit() for _ in range(size)]
    for i in q:
        ops.H | i
    for i in q:
        ops.Measure | i
    eng.flush()

def qrack(size):
    subprocess.call(['qrack/build/examples/h', str(size)])
    
def quest(size):
    subprocess.call(['QuEST/h' , str(size)])

h = {'QSystem' : qsys,
     'Cirq' : crq,
     'Forest' : forest,
     'ProjectQ' : projq,
     'Qiskit' : qkit, 
     'Qrack' : qrack,
     'QuEST' : quest,
     } 

def call_plots():
    plot('h.pdf', h, list(range(2, 25)))
