
#include <algorithm> // std::random_shuffle
#include <cstddef> // size_t
#include <iostream> // std::cout

#include "qfactory.hpp"

using namespace Qrack;

int main(int argc, char* argv[]) {
    
    int size = std::atoi(argv[1]);
    QInterfacePtr qReg = CreateQuantumInterface(QINTERFACE_QUNIT, QINTERFACE_OPTIMAL, size, 0);
    
    for (int i = 0; i < size; i++)
        qReg->H(i);
    
    for (int i = 0; i < size; i++)
        qReg->M(i);

    return 0;
}
