import plotly.graph_objects as go
from statistics import mean
from time import time
import gc

dash_types = ["solid", "dot", "dash", "longdash", "dashdot","longdashdot", "2px"]

def trace(name, func, dash, rang):
    print('BEGIN', name, end=' ')
    times = []
    for size in rang:
        print(size, end=' ')
        avg = []
        for _ in range(5):
            gc.collect()
            start = time()
            func(size) 
            end = time()
            avg.append(end-start)
        times.append(mean(avg))

    print('END')

    return go.Scatter (
        name=name,
        x=rang,
        y=times,
        line_width=3,
        mode="lines",
        line_dash=dash,
    )

def layout(fig):
    fig.update_layout(plot_bgcolor='rgba(0,0,0,0)',
                      yaxis_type='log',
                      yaxis_title='Time (s)',
                      xaxis_dtick=2,
                      font_size=22,
                      legend_orientation="h",
                      legend_y=1,
                      legend_yanchor="bottom",
                      legend_xanchor="left",
                      font_color="black",
                      xaxis_range=[4, 24],
                      xaxis_title='Number of Qubits',
                      xaxis_gridcolor='LightGray',
                      yaxis_gridcolor='LightGray',
                      )

    fig.update_xaxes(showline=True, linewidth=1, linecolor='black', mirror=True)
    fig.update_yaxes(showline=True, linewidth=1, linecolor='black', mirror=True)

def layout_right(fig):
    fig.update_layout(yaxis_side="right")

def plot(name, funcs, rang):
    fig = go.Figure()
    for key, dash in zip(funcs, dash_types):
        fig.add_trace(trace(key, funcs[key], dash, rang))
    layout(fig)
    fig.write_image(name)
    #fig.show() 
